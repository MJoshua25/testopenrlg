
<?php
session_start();
require_once "db/db_connect.php";
require_once 'models/personne.model.php';
require_once "db/personne.php";

$errMsg = '';
if (isset($_POST['username'])) {
	$email = $_POST['username'];
	$motdepasse  = $_POST['password'];
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$errMsg = "L'adresse email n'est pas valide.";
		$_SESSION['isLogin'] = 'false';
	} else {
		$isConnect = isPersonneConnecte($email, $motdepasse);
		if ($isConnect) {
			$personne = getPersonneEmail($email);
			$_SESSION['isLogin'] = "isOk";
			$_SESSION['email'] = $personne[0]["email"];
			$_SESSION['nom'] = $personne[0]["nom"];
			$_SESSION['prenom'] = $personne[0]["prenom"];
			$_SESSION['role'] = $personne[0]["role"];
			$_SESSION['token_mdp'] = $personne[0]["token_mdp"];
			
			header("Location: index.php");
			exit;
		} else {
			$_SESSION['isLogin'] = 'false';
			$errMsg = 'Login ou mot de passe incorrect</b></span>';
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V2</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="data/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="data/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="data/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="data/css/util.css">
	<link rel="stylesheet" type="text/css" href="data/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="login.php" method="post">
					<span class="login100-form-title p-b-26">
						bienvenue
					</span>
					<span class="login100-form-title p-b-48">
						<img src="assets/img/logo_rlg.png" alt="Louis_Guilloux" class="logo_standard" />
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type='text' name='username' id='username' />
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Entrer mot de passe">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type='password' name='password' id='password' />
						<span class="focus-input100" data-placeholder="mot de passe"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type='submit' name='btn_login'>
								entrer
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							
						</span>

						<a class="txt2" href="forgetmdp.php">
							mot de passe oublié?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>