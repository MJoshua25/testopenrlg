<?php
session_start();
if (!isset($_SESSION['isLogin']) || (isset($_SESSION['isLogin']) && $_SESSION['isLogin'] != "isOk")) {    
    header("Location: login.php");
    exit;
}
if(isset($_SESSION['isLogin']) && isset($_SESSION['token_mdp']) && 
    empty($_SESSION['token_mdp'])){
    header("Location: initmdp.php");
    exit;
}
$isAccess = ($_SESSION['role']=="ADMIN" || $_SESSION['role']=="RPINT");
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.png" type="image/png">
	<title>Accueil</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="vendors/linericon/style.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="css/magnific-popup.css">
	<link rel="stylesheet" href="vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="vendors/animate-css/animate.css">
	<link rel="stylesheet" href="vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand logo_h" href=""><img src="assets/img/logo_rlg.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
						<ul class="nav navbar-nav menu_nav justify-content-center">
							<li class="nav-item active"><a class="nav-link" href="#"><?php echo ucfirst($_SESSION['prenom']) . ' ' . strtoupper($_SESSION['nom']); ?></a></li>
							
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="nav-item"><a href="logout.php" class="primary_btn">Déconnexion</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

	

	<!--================Start About Us Area =================-->
	
	<!--================End About Us Area =================-->

	<!--================First Upcoming Games Area =================-->
	<section class="upcoming_games_area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="main_title">
						<h2>rubriques</h2>
						<h1> BIENVENUE SUR OPENRLG, L'application de gestion du système d'information du réseau Louis Guilloux</h1>
					</div>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/structures.png" alt="">
						<div class="upcoming_title">
							<h3><a href="structures/">Gérer les structures</a></h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/langues.jpg" alt="">
						<div class="upcoming_title">
							<h3><a href="langues/">Gérer les langues</a></h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/patients.jpg" alt="">
						<div class="upcoming_title">
							<h3><a href="personnes/">Gérer les personnes</a></h3>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/interventions.jpeg" alt="">
						<div class="upcoming_title">
							<h3><a href="">Gérer les interventions</a></h3>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/agendas.jpg" alt="">
						<div class="upcoming_title">
							<h3><a href="">Gérer les agendas</a></h3>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/import_export.jpg" alt="">
						<div class="upcoming_title">
							<h3><a href="">Import/Export</a></h3>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/tableau_bord.png" alt="">
						<div class="upcoming_title">
							<h3><a href="">Tableau de bord</a></h3>
						</div>
					</div>
				</div>

				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="new_games_item">
						<img src="assets/img/mon_compte.jpeg" alt="">
						<div class="upcoming_title">
							<h3><a href="moncompte/">Gérer mon compte</a></h3>
						</div>
					</div>
				</div>


			</div>
		</div>
	</section>
	<!--================End Upcoming Games Area =================-->

	


	<!--================ Start Pricing Plans Area ================-->
	
	</footer>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="vendors/isotope/isotope-min.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="js/jquery.ajaxchimp.min.js"></script>
	<script src="vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="js/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/theme.js"></script>
</body>

</html>