<?php
session_start();
if (!isset($_SESSION['isLogin']) || (isset($_SESSION['isLogin']) && $_SESSION['isLogin'] != "isOk")) {    
    header("Location: login.php");
    exit;
}
if(isset($_SESSION['isLogin']) && isset($_SESSION['token_mdp']) && 
    empty($_SESSION['token_mdp'])){
    header("Location: initmdp.php");
    exit;
}
$isAccess = ($_SESSION['role']=="ADMIN" || $_SESSION['role']=="RPINT");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Accueil</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area wow fadeInDown" data-wow-delay="500ms" style="background: #ffa500">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex align-items-center justify-content-between">
                        <!-- Logo Area -->
                        <div class="logo">
                            <a href="index.php"><img src="assets/img/logo_rlg.png" alt=""></a>
                        </div>

                        <!-- Search & Login Area -->
                        <div class="search-login-area d-flex align-items-center">
                            <!-- Top Search Area -->
                            
                            <!-- Login Area -->
                            <div class="login-area">
                                <a href="logout.php"><span>Déconnexion</span> <i class="fa fa-lock" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        
        
    </header>
    <!-- ##### Header Area End ##### -->

    <!-- ##### Hero Area Start ##### -->
   

    <!-- ##### Games Area Start ##### -->
    <div class="games-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
                        <img src="assets/img/structures.png" alt="">
                        <a href="structures/" class="btn egames-btn mt-30">Gérer les structures</a>
                    </div>
                </div>

                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="300ms">
                        <img src="assets/img/langues.jpg" alt="">
                        <a  href="langues/" class="btn egames-btn mt-30">Gérer les langues</a>
                    </div>
                </div>

                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img  src="assets/img/patients.jpg" alt="">
                        <a href="personnes/" class="btn egames-btn mt-30">Gérer les personnes</a>
                    </div>
                </div>
                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img src="assets/img/interventions.jpeg" alt="">
                        <a href="#" class="btn egames-btn mt-30">Gérer les interventions</a>
                    </div>
                </div>
                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img src="assets/img/agendas.jpg" alt="">
                        <a href="#" class="btn egames-btn mt-30">Gérer les agendas</a>
                    </div>
                </div>
                <!-- Single Games Area -->
                <div class="col-12 col-md-4">
                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img src="assets/img/import_export.jpg" alt="">
                        <a href="#" class="btn egames-btn mt-30">Import/Export</a>
                    </div>
                </div>
                 <div class="col-12 col-md-4">

                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img src="assets/img/tableau_bord.png" alt="">
                        <a href="#" class="btn egames-btn mt-30">Tableau de bord</a>
                    </div>
                </div>

                <div class="col-12 col-md-4">

                    <div class="single-games-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <img img src="assets/img/mon_compte.jpeg" alt="">
                        <a href="moncompte/" class="btn egames-btn mt-30">Gérer mon compte</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
  
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>